package Lesson3.Car.service;

import Lesson3.Car.dto.CarDto;
import Lesson3.Car.dto.CreateCarDto;

import java.util.List;

public interface ICarService {
    public void createCar(CreateCarDto createTeacherDto);
    public CarDto getCarById(long id);
    public List<CarDto> getAllCars();
    public void updateCar(long id, CreateCarDto createTeacherDto);
    public String deleteCar(long id);

}
