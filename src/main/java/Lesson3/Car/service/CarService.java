package Lesson3.Car.service;

import Lesson3.Car.dto.CarDto;
import Lesson3.Car.dto.CreateCarDto;
import Lesson3.Car.model.Car;
import Lesson3.Car.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarService implements ICarService {

    private final ModelMapper modelMapper;
    private final CarRepository carRepository;

    public CarService(ModelMapper modelMapper, CarRepository carRepository) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void createCar(CreateCarDto createCarDto) {
        Car car = modelMapper.map(createCarDto, Car.class);
        carRepository.save(car);
    }

    @Override
    public CarDto getCarById(long id) {
        Car car = carRepository.findById(id).orElse(null);
        return modelMapper.map(car, CarDto.class);
    }

    @Override
    public List<CarDto> getAllCars() {
        List<Car> cars = carRepository.findAll();
        List<CarDto> carDtos = new ArrayList<>();
        cars.forEach(car ->
                carDtos.add(modelMapper.map(car, CarDto.class)));
        return carDtos;
    }

    @Override
    public void updateCar(long id, CreateCarDto createTeacherDto) {
        Optional<Car> car = carRepository.findById(id);
        car.ifPresent(car1 -> {
            car1.setBanNumber(createTeacherDto.getBanNumber());
            car1.setCarColor(createTeacherDto.getCarColor());
            car1.setEngine(createTeacherDto.getEngine());
            car1.setModel(createTeacherDto.getModel());
            car1.setMaker(createTeacherDto.getMaker());
            car1.setYear(createTeacherDto.getYear());
            carRepository.save(car1);
        });
    }

    @Override
    public String deleteCar(long id) {
        carRepository.deleteById(id);
        return "Car is deleted";
    }
}
