package Lesson3.Car.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
@Data

public class CreateCarDto {
    private short banNumber;
    private String carColor;
    private String engine;
    private String model;
    private String maker;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate year;
}
