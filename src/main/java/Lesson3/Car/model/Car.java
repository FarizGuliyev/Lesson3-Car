package Lesson3.Car.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Data
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private short banNumber;
    private String carColor;
    private String engine;
    private String model;
    private String maker;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate year;
}
