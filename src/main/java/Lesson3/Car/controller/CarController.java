package Lesson3.Car.controller;

import Lesson3.Car.dto.CarDto;
import Lesson3.Car.dto.CreateCarDto;
import Lesson3.Car.model.Car;
import Lesson3.Car.service.ICarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Car")
public class CarController {
    private final ICarService carService;

    public CarController(ICarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto createCarDto) {
        carService.createCar(createCarDto);
    }

    @GetMapping("/{id}")
    public CarDto getCarById(@PathVariable long id) {
        return carService.getCarById(id);
    }

    @GetMapping
    public List<CarDto> getAllCars() {
        return carService.getAllCars();
    }

    @PutMapping("/{id}")
    public void updateCar(@PathVariable long id, @RequestBody CreateCarDto createCarDto) {
        carService.updateCar(id, createCarDto);
    }

    @DeleteMapping("/{id}")
    public String deleteCar(@PathVariable long id){
        return carService.deleteCar(id);
    }
}
